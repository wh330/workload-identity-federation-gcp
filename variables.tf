# Terraform module variables.

variable "gitlab_url" {
  type    = string
  default = "https://gitlab.developers.cam.ac.uk"
}

variable "gitlab_project_id" {
  type        = string
  description = "Project ID to restrict authentication from."
}

variable "gitlab_namespace_path" {
  type        = string
  description = "Namespace Path to Filter Auth Requests"
}

variable "gcp_project_name" {
  type        = string
  description = "GCP Project name"
}

variable "gitlab_token" {
  type        = string
  description = "GitLab Access Token"
}

# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.14.0"
  constraints = "~> 3.6"
  hashes = [
    "h1:xR1R5xT0jLXzRqqkHLh8fQ6MjHSwvw1qajFulebHHlY=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:25be08eb83e94820d72884a02684dd38b8a47544dc5afce1640ba25a94720c6f",
    "zh:2a0b9388a1b7c84afce36df990b4f23de50fb5672538333f46998d14319f213a",
    "zh:2bdf18cbd144326fc99cf507196ad21d099f0360c16786fb543d575d6770a139",
    "zh:487890bc773edb3e42831ce649eb627a8a21da0b8874ae068e204c6fb0e834a3",
    "zh:4e47b4430f9974050188cb09fb9ac4647eb473b04d50165277b290e5e742b6cf",
    "zh:63735a191a39b177eb4513cea9f4592971a017d5d3bf7942b39d955e960c819a",
    "zh:6f89bbc30eabc4ae79af0bda57d7d5c037e68c24130348780673662a4fa2bd10",
    "zh:7d42633babb29f8d86df0e03b1b8738a16f9cba61d3951aca02e32174a11299f",
    "zh:865d20d3e868ac8ece7fe4294eaca9c85945400e043b74003ef01606bcfb8092",
    "zh:a747a4b55f25982896939785acba21ac441c8eaf1b906f1bb1dc7388b4f5350b",
    "zh:b0b2217b2aa97a1aea44d1f58463699663c4bd8832621c36c37617cada8b1ff5",
    "zh:d48bfcf224c21756a35e150a1cb2cf699ba7ebffc575bf4dd2297e918e82da12",
    "zh:fa64065c7f37ce4c9052ba9c78db632e2b55806ca53a969febcaa17d210f9644",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.70.0"
  constraints = "~> 3.70.0"
  hashes = [
    "h1:JPwfdswdjpDus5o+1w2IG9NJUY1rcwJHvBhuf5SE0L0=",
    "zh:26f37fc308ddb20baf20efd93726b2ff7894310c4980c07f3e0ae467ff3cad82",
    "zh:40cd363b7a325833685940b5fd6d6ba5a54d1a637dba06ae05114facdf7f49a9",
    "zh:42ee807cba7f0e1c06b52b3a70ee5da707a38d7a73a459e99cadd733a38f53a5",
    "zh:57d6fd677c699be7ae97cfcd831283e2d04b1e168c9906ab49a499663ba0c801",
    "zh:7238128698516b9a6f7d49b1f772aeee0234e162997ca5fd16315c6a57c8fead",
    "zh:77d923faac5dd9744a4e0ba4d47a8b2de19358fff9b2060b82b127694a48c9d2",
    "zh:7fdfb1b0bce09bbae8ab4d6c44d72ddbaddddf14c6aca3d952f71e03a57d9d0d",
    "zh:a14af8edad375b15502cb33c2ac9a401b14c891832b4257056d86a4f65a453f7",
    "zh:c8a7b2202db3ffaad11011911181a382f3b55a0804d3c0a1177e6431a391e426",
    "zh:cd0818982ee24c8bd1caed93816b6f15fa1cef07de39d1edf5110fd17e892430",
    "zh:d2abded6c1088a85d7487369998c71652a338b46b1646e67676a717ff1f394f8",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "3.70.0"
  constraints = "~> 3.70.0"
  hashes = [
    "h1:I8SzEPfS+2YWBleaW3G6x5bQLp1vQDMABC/O+z3fKIA=",
    "zh:13edced3fe97a18cffa3aa328bd6870405ba97e8ee2cf8a13e9d6504c52f2da3",
    "zh:27ffcf849714797595267fc4fed2fa5a1aafe8b5c8a0c3ef0c44686bf706f368",
    "zh:34890ba5a4fa5bdcaa72cde46fa55685e90247bbb11604f255de55101a0e4120",
    "zh:5f856657d7832259113e56609b29f793d37c2489b4e0c026d56c5bd6ae7c386a",
    "zh:850e535f1117db0ac25fc45520d4500c8f4c1ae6e5ccd5a4e02d9efb8ceb16ce",
    "zh:8a7e43bfa06634f2fd5a9202c8cbe4d0ac8140f0801e45f79001bbc2768f6c08",
    "zh:8d71f11ce543833a6ee23561515e432919dcfffd4929b690ca12b6dfb6e47f9b",
    "zh:b716a28a63f6308b10ef37b2cae0a701e24675121f7a2f7f014f937b34abdfb9",
    "zh:e02194ec55361231f758b73e50915a0cab64090402fcd30a97100ad575fb1243",
    "zh:e234119f0fa3f001d8a33343ab36aeb675d903b50fe08d882bfc775cf9f4cebd",
    "zh:eeb897ef7770b50e5cab0e65c4c93be871ef8ff8519574c394d9d503d7ac7dbb",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.3"
  hashes = [
    "h1:LPSVX+oXKGaZmxgtaPf2USxoEsWK/pnhmm/5FKw+PtU=",
    "zh:26e07aa32e403303fc212a4367b4d67188ac965c37a9812e07acee1470687a73",
    "zh:27386f48e9c9d849fbb5a8828d461fde35e71f6b6c9fc235bc4ae8403eb9c92d",
    "zh:5f4edda4c94240297bbd9b83618fd362348cadf6bf24ea65ea0e1844d7ccedc0",
    "zh:646313a907126cd5e69f6a9fafe816e9154fccdc04541e06fed02bb3a8fa2d2e",
    "zh:7349692932a5d462f8dee1500ab60401594dddb94e9aa6bf6c4c0bd53e91bbb8",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9034daba8d9b32b35930d168f363af04cecb153d5849a7e4a5966c97c5dc956e",
    "zh:bb81dfca59ef5f949ef39f19ea4f4de25479907abc28cdaa36d12ecd7c0a9699",
    "zh:bcf7806b99b4c248439ae02c8e21f77aff9fadbc019ce619b929eef09d1221bb",
    "zh:d708e14d169e61f326535dd08eecd3811cd4942555a6f8efabc37dbff9c6fc61",
    "zh:dc294e19a46e1cefb9e557a7b789c8dd8f319beca99b8c265181bc633dc434cc",
    "zh:f9d758ee53c55dc016dd736427b6b0c3c8eb4d0dbbc785b6a3579b0ffedd9e42",
  ]
}

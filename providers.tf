# Used providers

provider "gitlab" {
  token    = var.gitlab_token
  base_url = var.gitlab_url
}

provider "google" {
  project = var.gcp_project_name
  region  = local.region
  zone    = local.zone
}

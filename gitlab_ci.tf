# Defines GitLab CI resources

# Create GitLab CI/CD variables
resource "gitlab_project_variable" "POOL_ID" {
  project = var.gitlab_project_id
  key     = "POOL_ID"
  value   = "gitlab-pool-${random_id.random.hex}"
}

resource "gitlab_project_variable" "PROVIDER_ID" {
  project = var.gitlab_project_id
  key     = "PROVIDER_ID"
  value   = "gitlab-pool-provider-${random_id.random.hex}"
}

resource "gitlab_project_variable" "SERVICE_ACCOUNT_EMAIL" {
  project = var.gitlab_project_id
  key     = "SERVICE_ACCOUNT_EMAIL"
  value   = google_service_account.gitlab-runner.email
}

resource "gitlab_project_variable" "PROJECT_NUMBER" {
  project = var.gitlab_project_id
  key     = "PROJECT_NUMBER"
  value   = data.google_project.project.number
}

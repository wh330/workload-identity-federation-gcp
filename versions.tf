# versions.tf specifies minimum versions for providers and terraform.

terraform {
  # Specify the required providers, their version restrictions and where to get
  # them.
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.70.0"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 3.70.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.6"
    }
  }

  required_version = "~> 1.0"
}

# Creates and sets the permissions of the impersonated service account.

resource "google_service_account" "gitlab-runner" {
  account_id   = "gitlab-runner-service-account"
  display_name = "Service Account for GitLab Runner"
}

resource "google_service_account_iam_binding" "gitlab-runner-oidc" {
  provider           = google-beta
  service_account_id = google_service_account.gitlab-runner.name
  role               = "roles/iam.workloadIdentityUser"

  members = [
    "principalSet://iam.googleapis.com/${google_iam_workload_identity_pool.gitlab-pool.name}/attribute.project_id/${var.gitlab_project_id}"
  ]

}
